import java.util.Scanner;
/**
 * lop DictionaryCommandline ke thua lop DictionaryManagement
 * co tac dung thao tac voi tu dien
 * @author Pham Thanh Hung, Nguyen Trong Lam
 * @version 1.0 - 2.0- 3.0
 * @since 14/10/2018
 */
public class DictionaryCommandline extends DictionaryManagement{
    /*
    ham showAllWord co chuc nang in ra man hinh tu tieng anh va tieng viet
    */
	public static void showAllWords() {
		System.out.printf("%-4s%-1s%-20s%-1s%-8s%n","No","|","English","|","Vietnamese");
		for(int i=0;i<NumberOfWords;i++) {
			System.out.printf("%-4d%-1s%-20s%-1s%-8s%n",i+1,"|",words[i].getWord_target(),"|",words[i].getWord_explain());
		}
	}
        /*
        ham dictionaryBasic de goi 2 ham showAllWord va insertFromCommandline
        */
	public static void dictionaryBasic() {
		insertFromCommandline();
		showAllWords();
	}
        /*
        ham dictionaryAdvanced de goi 2 ham insertFromFile va showAllWord
        */
	public static void dictionaryAdvanced() {
		insertFromFile();
		//System.out.println(dictionaryLookup());
		showAllWords();
	}
       /**
        * DictionaryInputWordByWord() la ham nhap tu vao trong tu dien va in ra file
        */
        public static void DictionaryInputWordByWord(){
            InputWordByWord();// nhap tu tung vao Arraylist
            FixOrReplaceWord();// sua tu 
            RemoveWord();// xoa tu
            dictionarySearcher();// tim tu theo theo chu cai nhap vao
            dictionaryExportToFile();// in ra File
        }
         /**
         * ham main dung de goi cac ham
         * @param ags khong duoc su dung
         */
	public static void main(String[] ags) {
            /*
            chon 1 ham can goi tuong ung voi moi chuc nang cua tu dien
            */
            //dictionaryBasic();
            //dictionaryAdvanced();
            //DictionaryInputWordByWord();
	}
}