import java.util.Scanner;
import java.util.ArrayList;
import java.io.*;
import java.util.*;
import java.io.File;
import java.util.Formatter;
import java.io.BufferedReader;
import java.net.URL;
import java.io.BufferedWriter;
import java.io.FileWriter;
/**
 * Lop DictionaryManagement ke thua lop Dictionary
 * @author Pham Thanh Hung, Nguyen Trong Lam
 * @version commandline
 */
public class DictionaryManagement extends Dictionary {
    // ham insertFromCommandline co chuc nang nhap tu tieng anh va tu tieng viet tu ban phim
	public static void insertFromCommandline() {
		Scanner scan=new Scanner(System.in);  
		// nhap so luong tu
                System.out.println("Input Number Of Words"); 
		NumberOfWords=scan.nextInt(); 
		scan.nextLine();
		SetUp(NumberOfWords);
                // su dung vong for de nhap tu trong mang Word
		for(int i=0;i<NumberOfWords;i++) {
			words[i] = new Word();
                        // nhap tu tieng anh
			System.out.println("Input English Words");
			words[i].setWord_target(scan.nextLine());
                        // nhap tu tieng viet
			System.out.println("Input Vietnamese Words");
			words[i].setWord_explain(scan.nextLine());
		}
		scan.close(); // dong scanner
	}
	static ArrayList<Word> List=new ArrayList<>(); // tao ra 1 Arraylist dung de them, sua, xoa tu.
	static Scanner dfile;
        static URL url = DictionaryManagement.class.getResource("dictionary.txt"); // tao ra 1 duong dan truu tuong den file dictionary.txt
	// ham OpenFile dung de kiem tra xem co mo duoc file khong
        public static void OpenFile() {
		try {
		 dfile =new Scanner(url.openStream());
		}
                // neu khong tim duoc file se bao loi
		catch (Exception e) {
			System.out.println("error try to open not existing file");
		}
	}
        // ham ReadFile co chuc nang doc file
	public static void ReadFile() {
               	int i=0;
                try{
                // BufferedReader duoc su dung  de doc du lieu tu file dictionary.txt
                BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
                            int lines = 0;
                            while (reader.readLine() != null) lines++;
                            reader.close(); // dong buffered
                            // so dong trong file tuong ung voi so tu trong mang Word
                            SetUp(lines);
                }
                // neu thao tac sai se bao loi
                catch(Exception e){
                    System.out.println("Error File");
                }
                
                try{
                    // doc file va dua du lieu vao mang Word words
                    while(dfile.hasNext()) {
                            words[i]=new Word();
                            words[i].setWord_target(dfile.next());
                            words[i].setWord_explain(dfile.nextLine());
                            i++;
                            
                }
                    NumberOfWords=i;
                }
                /*
                neu cac lenh trong try khong dung se bi bao loi
                */
                catch(Exception e){
                        System.out.println("Error File next");
                        }
               
        }
              
	// ham CloseFile dung de dong file sau khi su dung
	public static void CloseFile() {
		dfile.close();
	}
        // ham insertFromFile bao gom 3 ham mo, doc va dong file
	public static void insertFromFile() {
		OpenFile();
		ReadFile();
		CloseFile();
	}
        // ham dictionaryLookup co chuc nang tra tu
	public static String dictionaryLookup() {
		
		Scanner scan=new Scanner(System.in);
                // nhap tu can tim
		System.out.println("Input The Word Look Up");
	    String WordForLookUp=scan.next(); // gan tu can tim la WordForLookUp
            /* so sanh tu can tim voi tu tieng anh trong mang Word
            * neu giong nhau thi in ra man hinh tu tieng viet
            */
		for(int i=0;i<NumberOfWords;i++) {
			if(words[i].getWord_target().compareTo(WordForLookUp)==0) {
				return words[i].getWord_explain();
			}
		}
		scan.close(); // dong scanner
                /*
                neu khong tim duoc tu can tim ham se tra ve theo kieu String
                */
		return "Not Found The Word ";
	}
        /*
        ham inputWordByWord dung de them tu vao file word cung nhu List
        */
	public static void InputWordByWord() {
		System.out.println("Input 'Word' For Dictionary / Input 'Exita' To Stop");
		Scanner scan=new Scanner(System.in);
		try {
			/*nhap tu tu ban phim
                        neu nhap tu "Exita" thi se thoi khong nhap nua
                    con ko phai tu "Exita" thi se them tu minh nhap vao file text
                    */
			while(true) {
			String temp=scan.nextLine();
			if(temp.compareTo("Exita")==0) {
				break;
			}
			Word w=new Word();
                        // nhap tu tieng anh can them 
			System.out.println("Input English Word Update");
			String teE=scan.nextLine();
			w.setWord_target(teE);
                        // nhap tu tieng viet can them
			System.out.println("Input VietNamese Word Update");
			String teV=scan.nextLine();
			w.setWord_explain(teV);
                        // dung arrayList de them 2 tu tieng anh va tieng viet vao mang Word va file text
			List.add(w);
			System.out.println("Input 'Word' For Dictionary / Input 'Exita' To Stop");
		}
		}
                /*
                neu cac lenh trong try ko dung se bao loi
                */
		catch (Exception e) {
			System.out.println("Erro X");
		}
	}
        /**
        *ham FindWord dung de tim tu trong danh sach tu
        * @param w,e 2 tu dung de so sanh voi tu can tim
        * @return neu tim duoc tu ham tra ve vi tri cua tu trong danh sach tu
        * con khong thi ham tra ve -1
        */
	public static int FindWord(String w,String e) {
		for(int i=0;i<List.size();i++) {
			if(List.get(i).getWord_target().compareTo(w)==0 && List.get(i).getWord_explain().compareTo(e)==0) {
				return i;
			}
		}
		return -1;
	}
        /**
         * ham FixOrReplaceWord dung de sua tu
         */
	public static void FixOrReplaceWord() {
		Scanner scan=new Scanner(System.in);
                System.out.println("Input Words To Fix Include EaV(Exactly)");
                System.out.println("Input English Word");
		// nhap tu can sua
                if(scan.hasNextLine()) ;
		String TE=scan.nextLine();
                System.out.println("Input VietNamese Word");
		if(scan.hasNextLine()) ;
		String TV=scan.nextLine();
                // goi bien pos kieu so nguyen tuong ung voi vi tri tu can sua
		int pos=FindWord(TE,TV);
                /*
                neu pos = -1 suy ra khong co tu can tim
                neu khong thi sua tu can tim bang cach nhap tu moi tu ban phim
                */
		if( pos== -1) {
			System.out.println(" Not Find The Word");
		}
		else {
			System.out.println("Fix Data For Vietnamese");
				List.get(pos).setWord_explain(scan.nextLine());
			System.out.println("Fix Data for English");
				List.get(pos).setWord_target(scan.nextLine());
		}
}       /**
        * ham RemoveWord co chuc nang xoa tu 
        */
	public static void RemoveWord() {
		Scanner scan=new Scanner(System.in);
		System.out.println("Input Words To Remove Include EaV(Exactly)");
		// nhap tu can xoa
                System.out.println("Input English Word");
                String TE=scan.nextLine();
                System.out.println("Input VietNamese Word");
		String TV=scan.nextLine();
                // goi bien pos kieu so nguyen tuong ung voi vi tri tu can sua
                int pos=FindWord(TE,TV);
		/*
                neu pos = -1 suy ra khong co tu can tim
                neu khong thi xoa tu vua nhap tu ban phim
                */
                if( pos== -1) {
			System.out.println(" Not Find The Word");
			System.exit(0);
		}
		List.remove(pos);
		
	}
        /* ham dictionarySearcher co chuc nang tra tu theo cach thuan tien hon
	*/
            public static void dictionarySearcher() {
            // nhap tu can tim
		Scanner scan=new Scanner(System.in);
		String str=scan.nextLine();
		for(int i=0;i<List.size();i++) {
                    /* xet so luong ki tu minh vua nhap
                    * neu cac tu trong List co so ki tu lon hon so ki tu vua nhap
                    * va xet tu ki tu dau tien neu giong voi cac ki tu minh nhap vao
                    * thi in nhung tu do ra man hinh
                    */
                    System.out.println("Input Characters To Search");
			if(List.get(i).getWord_target().length() >= str.length()) {
				if (List.get(i).getWord_target().substring(0, str.length()).compareTo(str)==0) {
					System.out.println(List.get(i).getWord_target());
				
				}
			}
		}
	}
            // ham OpenFormatFile co chuc nang xuat du lieu ra file text
	public static void OpenFormatFile() {
            try {
                //System.out.println(urlw.getPath());
                    File file = new File("ExportFileFromCommandline.txt");
                   FileWriter fw = new FileWriter(file);
                  // System.out.println(List.size());
                   for(int i=0;i<List.size();i++){
                     fw.write(List.get(i).getWord_target()+"\t"+List.get(i).getWord_explain()+"\n");
                   }
                   System.out.println(" File Has Been Exported");
                  fw.close();
                   fw.close();
		}
		catch (Exception e) {
			System.out.println("Erro Opening Formatter File");
		}
	}
/**
 * ham de goi export ra file
 */
	public static void dictionaryExportToFile() {
		OpenFormatFile();
		
	}
}