/**
 * Lop Word la lop co 2 thuoc tinh word_target va word_explain
 * @author Pham Thanh Hung, Nguyen Trong Lam
 * @version commandline
 * 14/10/2018
 */
public class Word {
	private String word_target; // tu can tim(tieng anh)
	private String word_explain; // tu giai thich(tieng viet)
        // cac ham get/set thuoc tinh cua lop Word
	public void setWord_target(String sw) {
		word_target=sw;
	}
	public String getWord_target() {
		return word_target;
	}
	public void setWord_explain(String swe) {
		word_explain=swe;
	}
	public String getWord_explain() {
		return word_explain;
	}
}